import json
import pika
from fastapi import FastAPI
from pydantic import BaseModel
from typing import Dict

app = FastAPI()
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
channel.queue_declare(queue='tasks')


class Task(BaseModel):
    task_id: str
    title: str
    params: Dict[str, str] = {}


@app.post("/", response_model=Task)
def add_task(task: Task):
    message = task.dict()
    channel.basic_publish(
        exchange='',
        routing_key='task_queue',
        body=json.dumps(message),
        properties=pika.BasicProperties(
            delivery_mode=2
        )
    )
    return task
