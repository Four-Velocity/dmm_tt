import json
import pika
from pprint import pprint


def callback(ch, method, properties, body):
    pprint(f'New task: {json.loads(body)}')


if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.queue_declare(queue='tasks')
    channel.basic_consume(
        queue='tasks',
        on_message_callback=callback,
        auto_ack=True
    )
    channel.start_consuming()
